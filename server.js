const express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')
const app = express()

app.use(express.urlencoded({ extended: false }))

// parse application/json
app.use(express.json())
app.use(cors())

const routes = require('./app/routers/routers')
app.use(routes)

const conn = require('./database');

app.listen(8000, () => {
  console.log('Running on port 8000')
})
