const conn = require('../../database')
exports.list = (req,res) => {
  conn.query("select * from purchase join stock on stock.id = purchase.stock_id join sales on sales.stock_id = stock.id",
  (err,data) => {
    if(data){
      res.status(200).json({ data: data })
    }
  })
}

exports.addStock = (req,res) => {
  let stockName = req.body.stockName
  conn.query("INSERT INTO stock (stockName) VALUES (?)", 
  [stockName], (err,data) => {
  if(err)
    res.json(err);
  else {
    res.status(201).json({ success: true }); 
  }
  })
}

exports.getStock = (req,res) => {
  conn.query("SELECT stockName from stock" , (err,data) => {
    res.status(200).json({ data: data })
  })
}

exports.addTransaction = (req,res) => {
  let stockName = req.body.stockName
  conn.query("SELECT id from stock where stockName = ?",
    [stockName], (err,data) => {
      let transactionType = req.body.transactionType
      if(transactionType === 'Buy'){
        let quantity = req.body.quantity
        let price = req.body.price
        let stock_id = data[0].id
        conn.query("INSERT INTO purchase (purchaseQuantity,purchasePrice,stock_id) VALUES (?,?,?)", 
          [quantity,price,stock_id], (err, data) => {
        if(err)
          res.json(err);
        else 
          res.status(201).json({ success: true });
      })
    } else if(transactionType === 'Sell'){
        let quantity = req.body.quantity
        let price = req.body.price
        let stock_id = data[0].id
        conn.query("INSERT INTO sales (sellQuantity,sellPrice,stock_id) VALUES (?,?,?)", 
          [quantity,price,stock_id], (err, data) => {
        if(err)
          res.json(err);
        else 
          res.status(201).json({ success: true});
      })
    }
  })
}

exports.dashboard = (req,res) => {
  conn.query(`select sum(purchaseQuantity) - sum(sellQuantity) as totalUnit,
   sum(sellPrice*sellQuantity) as soldAmount, 
   sum(sellPrice*sellQuantity) - sum(sellQuantity*purchasePrice) as profit,
   sum(purchaseQuantity*purchasePrice) as totalInvestment,
   sum(purchaseQuantity*purchasePrice)+sum(sellPrice*sellQuantity) - sum(sellQuantity*purchasePrice) as currentAmount
   from purchase join stock on stock.id = purchase.stock_id
   join sales on sales.stock_id = stock.id`,
  (err,data) =>  {
    if(data){
      res.status(200).json({ data:data })
    }
  })
}

exports.getIndividual = (req,res) => {
  let id = 11
  conn.query(`select sum(purchaseQuantity) - sum(sellQuantity) as totalUnit,
   sum(sellPrice*sellQuantity) as soldAmount, 
   sum(sellPrice*sellQuantity) - sum(sellQuantity*purchasePrice) as profit,
   sum(purchaseQuantity*purchasePrice) as totalInvestment,
   stockName,
   sum(purchaseQuantity*purchasePrice)+sum(sellPrice*sellQuantity) - sum(sellQuantity*purchasePrice) as currentAmount
   from purchase join stock on stock.id = purchase.stock_id
   join sales on sales.stock_id = stock.id group by stock.id`,[id] , (err,data) =>  {
    if(data){
      res.status(200).json({ data:data })
    }
  })
}
