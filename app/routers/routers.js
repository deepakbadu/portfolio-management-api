const express = require('express')
const router = express()
const portfolio = require('../controllers/portfolio.controller')

router.get('/', portfolio.list)
router.post('/stock',portfolio.addStock)
router.post('/add',portfolio.addTransaction)
router.get('/dashboard',portfolio.dashboard)
router.get('/individualStock',portfolio.getIndividual)
router.get('/stock', portfolio.getStock)

module.exports = router